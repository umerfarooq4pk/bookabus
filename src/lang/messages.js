import en from './translations/en.json';
import du from './translations/du.json';

export default {
    en,
    du
}