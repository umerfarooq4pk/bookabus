const axios = require('axios');

const base_url = process.env.VUE_APP_API_URL;

export async function post(url){
    const response = await axios.get(`${base_url}${url}`);
    return response.data;
}