import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import './assets/css/app.css';
import BackToTop from 'vue-backtotop';
import ToastPlugin from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-bootstrap.css';

import i18n from './lang';

const feather = require('feather-icons');
feather.replace();

// createApp(App)
// 	.use(router)
// 	.use(BackToTop)
// 	.use(ToastPlugin)
// 	.mount('#app');


const app = createApp(App);

app.use(router)
app.use(i18n)
app.use(BackToTop)
app.use(ToastPlugin)

// export const i18n = new VueI18n({
//   locale: 'en',
//   fallbackLocale: 'en',
//   messages
// });

app.mount('#app')



const appTheme = localStorage.getItem('theme');

// Check what is the active theme and change theme when user clicks on the theme button in header.
if (
	appTheme === 'dark' &&
	document.querySelector('body').classList.contains('app-theme')
) {
	document.querySelector('body').classList.add('bg-primary-dark');
} else {
	document.querySelector('body').classList.add('bg-secondary-light');
}
